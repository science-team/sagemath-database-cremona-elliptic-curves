Manin constants and optimal curves
----------------------------------

For all conductors (levels) N up to 60000, and for specific levels up
to 500000, we have computed the full modular symbol space for
Gamma_0(N), and not only the plus space, in order to determine which
curve in the isogeny class is the Gamma_0(N)-optimal one and to
determine the Manin constants.  Note that in many cases, including
isogeny classes of size 1 but only these, it is possible to rigorously
show which curve is optimal using only the information coming from the
plus space, and in almost all cases (see below for the exceptions) it
is possible to deduce that the Manin constant for the optimal curve is
1 even when there is more than one possible optimal curve.  The
justification for these claims is in the Appendix (written by me) to
"The Manin Constant" by Amod Agashe, Ken Ribet and William Stein [Pure
and Applied Mathematics Quarterly, Vol. 2 no.2 (2006), pp. 617-636.]

In the "Cremona labels" of the curves in each isogeny class, curve
number 1 is the Gamma_0(N)-optimal curve, with the following provisos:

  - in class 990h the optimal curve is 990h3 (and not 990h1).  This is
    due to an error in preparation of the data published in the first
    (1992) edition of the tables in Algorithms for Modular Elliptic
    Curves (CUP, see
    http://homepages.warwick.ac.uk/staff/J.E.Cremona/book/amec.html)
    which I decided not to change later, when the discrepancy was
    discovered.

  - for 16436 conductors above 400000 (as of 26 November 2019), the
    optimal curve has not yet been determined and is one of n curves
    in the class for some n between 2 and 6 inclusive.  Work is in
    progress to eliminate these ambiguous cases, by doing more modular
    symbol computations.  Note that the heuristics used to guess which
    curve is optimal from the information from the plus space only are
    such that I do not expect to find any isogeny classes where the
    optimal curve is not number 1.  This was not true before August
    2019, when for 10 isogeny classes of size 2 linked by 2-isogenies
    the heuristics had given the wrong result; the labels for these
    classes (235470bb, 235746u, 258482a, 265706a, 333270bu, 359282a,
    369194a, 375410g, 377034t, 389774b) were switched on 19 August
    2019.

    Of the 2164259 isogeny classes of conductor up to 500000, there
    are 64249 of conductor > 400000 in which the optimal curve has not
    yet been determined, and 359009 in which the optimal curve is
    certainly curve number 1:

          2 possibly optimal curves in  53394 classes
          3 possibly optimal curves in  10017 classes
          4 possibly optimal curves in    763 classes
          5 possibly optimal curves in     32 classes
          6 possibly optimal curves in     43 classes

Concerning the Manin constant, the methods described in the paper
(op.cit.)  have been used to show that the Manin constant is 1 for
every optimal curve.  The theoretical results on the Manin constant c
which we have used are: that c is an integer, that c=2 is impossible
when N is odd, and that c=3 is impossible unless N is a multiple of 3.

On the assumption that the optimal curve is number 1 in all isogeny
classes (other than 990h), the Manin constants of all curves in the
database have been computed.  They are almost all 1, even for
non-optimal curves, and are rarely greater than 3:

            Manin constant      Number of curves        Curves

                  1             3064436
                  2                 225
                  3                  41                 14a4, 14a6, ...
                  4                   2                 15a8, 17a4
                  5                   1                 11a3

The curves with Manin constant 2 and 3 are distributed by conductor as
follows:
             range        #{c=3}  #{c=2}

           1  < N <10^2:   11     24
          10^2< N <10^3:    5     17
          10^3< N <10^4:    6     29
          10^4< N <10^5:    9     60
          10^5< N <2*10^5:  7     34
        2*10^5< N <3*10^5:  1     27
        3*10^5< N <4*10^5:  1     22
        4*10^5< N <5*10^5:  1     12

Data files
----------

The data files in opt_man/ have one line for each curve where the last
two fields contain an "optimality code" and the Manin constant
(conditional on curve number 1 being optimal).  The optimality code is
0 for "certainly not optimal", 1 for "certainly optimal", and n>1 for
"one of n possibly optimal curves in its isogeny class".

Additional remarks
--------------------

1. For 48 classes (in the range N<=500000), there would be a
possibility that c=2 and that the optimal curve is the second one
listed, if we relied only on the information given by modular symbols
with sign +1.  These all have the same form: 2 curves in the class
with types 1,2 and aj=1,1 so either E1 is optimal with c=1 or E2 is
optimal with c=2.  The minimal period lattices of E1, E2 have the form
[2x,x+yi], [2x,2yi] with x,y positive real, and from the +1 modular
symbols we can only say that the projection of the period lattice of
the normalized newform onto the real line is generated by x; so the
optimal curve might conceivably have lattice [x,yi], implying that E2
is optimal with c=2.  But (in all these cases) we find that the
projection to the imaginary axis is generated by yi, and the lattice
type is 1 (non-rectangular) so the optimal curve's period lattice is
[2x,x+yi], while the other curve has Manin constant 2.

These classes are (including the 13 listed in the appendix to the
paper cited):

            62516a, 67664a, 71888e, 72916a, 75092a,
            85328d, 86452a, 96116a, 106292b,111572a,
            115664a, 121168e, 125332a;

            133972a, 144464a, 149012a, 150608j, 164852a,
            169808a, 171412c, 184916a, 188372a, 211664a,
            217172b, 219088b, 220916b, 236212b, 240116a,
            250064a, 256052a, 260116a, 280916a, 285172a,
            291664a, 300368a, 302516a, 306932a, 329492a,
            343412a, 345808a, 367252a, 377012b, 384464d,
            391892a, 401972a, 425168b, 446288a, 481652a.

In all of the above cases I have computed the full modular symbol
space to eliminate the second possibility.

2. I have also computed the modular degrees of all curves (not just
the optimal ones) using Mark Watkins's sympow program (either as a
stand-alone, or via Sage, or as implemented in Magma), which confirms
optimality of the first curve in each class conditional on Stevens's
conjecture that the Gamma_1(N)-optimal curve is the one with minimal
Faltings Height (i.e. the one whose period lattice is a sublattice of
all the others).
