ecdata
======

This is a repository of the data files containing the Cremona Database
of all elliptic curves over Q of bounded conductor. The data is
available under TheArtistic License 2.0: see the file LICENSE for details.

These files provide the source data for the elliptic curve pages at
http://www.lmfdb.org .

The current release is published as
http://dx.doi.org/10.5281/zenodo.161341

For a description of how most of the data here was computed, see the
ANTS 7 Proceedings at
http://www.math.tu-berlin.de/~kant/ants/proceedings.html .  For a
description of the algorithms used, see the book Algorithms for
Modular Elliptic Curves, published by CUP in 1992 (second edition
1997) and available online here:
http://www.warwick.ac.uk/staff/J.E.Cremona/book/fulltext/ .
